package com.jansi.soap;

import java.util.List;

import javax.jws.WebService;

import com.jansi.soap.business.ProductServiceImpl;
import com.jansi.soap.product.Product;

@WebService(endpointInterface = "com.jansi.soap.ProductCatalogInterface",portName = "TestMartCatalogPort",serviceName = "TestMartCatalogService")
public class ProductCatalog implements ProductCatalogInterface {
	
	ProductServiceImpl productService = new ProductServiceImpl();
	
	
	@Override
	public List<String> getProductCategories(){
		return productService.getProductCategories();
	}

	
	@Override
	public List<String> getProducts(String category) {
		return productService.getProduct(category);
	}
	
	
	@Override
	public boolean addProduct(String category,String product){
		return productService.addProduct(category, product);
	}
	
	
	@Override
	public List<Product> getProductV2(String category){
		return productService.getProductV2(category);
	}

	

}
