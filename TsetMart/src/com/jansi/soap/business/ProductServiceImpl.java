package com.jansi.soap.business;

import java.util.ArrayList;
import java.util.List;

import com.jansi.soap.product.Product;

public class ProductServiceImpl {
	
	List<String> bookList = new ArrayList<String>();
	List<String> musicList = new ArrayList<String>();
	List<String> movieList = new ArrayList<String>();
	
	public ProductServiceImpl(){
		
		bookList.add("Inferno");
		bookList.add("Joyland");
		bookList.add("The game of thrones");
		
		musicList.add("Random access memories");
		musicList.add("Night visions");
		musicList.add("Jukebox");
		
		movieList.add("Great and Powerful");
		movieList.add("Despicable me");
		movieList.add("Star");
		
		
	}
	
	public List<String> getProductCategories(){
	List<String> categories = new ArrayList<String>();
	categories.add("books");
	categories.add("music");
	categories.add("movies");
	return categories;
	}
	
	public List<String> getProduct(String category){
		switch(category.toLowerCase()) {
		case "books":
			return bookList;
		case "music":
			return musicList;
		case "movies":
			return movieList;
		}
		return null;
	}
	
	
	
	
	
	public boolean addProduct(String category,String product) {
		
			switch(category.toLowerCase()) {
			case "books":
				 bookList.add(product);
				 break;
			case "music":
				musicList.add(product);
				break;
			case "movies":
				movieList.add(product);
				break;
				default:
					return false;
			}
			return true;
		
	}

	public List<Product> getProductV2(String category) {
		List<Product> productList = new ArrayList<Product>();
		productList.add(new Product("Java Brains", "1234", 1000.90));
		productList.add(new Product("Geeks for geeks", "1235", 1500.90));
		return productList;
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
